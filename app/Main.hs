module Main where

import YNotPrelude

import VCS.CLI

import Options.Applicative

main :: IO ()
main = do
  cmd <- execParser (info (dvcsCLI <**> helper) (progDesc "A CLI for version control"))
  putStrLn (show cmd)
