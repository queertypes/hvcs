module VCS.CLI.Core where

import YNotPrelude

import VCS.CLI.Common

data VCSCommand
  = CloneCommand Repository (Maybe RelDir) (Maybe CloneOptions)
  | InitCommand (Maybe RelDir) (Maybe InitOptions)

  | AddCommand (Maybe PathSpec) (Maybe AddOptions)
  | MvCommand MoveSpec (Maybe MoveOptions)
  | ResetCommand ResetSpec (Maybe ResetOptions)
  | RmCommand RelFile (Maybe RmOptions)

  | BisectCommand BisectSubCommand
  | GrepCommand (Maybe PathSpec) (Maybe GrepOptions)
  | LogCommand (Maybe RevisionRange) (Maybe LogFile) (Maybe LogOptions)
  | ShowCommand (Maybe Object) (Maybe ShowOptions)
  | StatusCommand (Maybe PathSpec) (Maybe StatusOptions)

  | BranchCommand BranchSubCommand
  | CheckoutCommand CheckoutSubCommand
  | CommitCommand (Maybe [UnknownPath]) (Maybe CommitOptions)
  | DiffCommand DiffSubCommand
  | MergeCommand MergeSubCommand
  | RebaseCommand RebaseSubCommand
  | TagCommand TagSubCommand

  | FetchCommand FetchSubCommand
  | PullCommand (Maybe PullSpec) (Maybe PullOptions)
  | PushCommand PushSubCommand
  deriving (Show, Eq)

--------------------------------------------------------------------------------
-- Sub Commands
--------------------------------------------------------------------------------
data BisectSubCommand
  = BisectStart (Maybe BisectStartOptions)
  | BisectBad (Maybe Revision)
  | BisectGood (Maybe Revision)
  | BisectTerms TermMark
  | BisectSkip SkipSpec
  | BisectReset Commit
  | BisectView
  | BisectReplay LogFile
  | BisectLog
  | BisectRun ShellCommand
  | BisectHelp
  deriving (Show, Eq)

data BranchSubCommand
  = BranchList
  | BranchTrack
  | BranchSetUpstream
  | BranchUnsetUpstream
  | BranchRename
  | BranchCopy
  | BranchDelete
  | BranchEditDescription
  deriving (Show, Eq)

data CheckoutSubCommand
  = CheckoutBranch (Maybe Branch)
  | CheckoutBranchDetached (Maybe Branch)
  | CheckoutBranchAtCommit Commit
  | CheckoutCreateBranch Branch (Maybe StartPoint)
  | CheckoutTreeish PathSpec
  | CheckoutPatchMode (Maybe Treeish) (Maybe [UnknownPath])
  deriving (Show, Eq)

data DiffSubCommand
  = DiffCurrent (Maybe [UnknownPath])
  | DiffStaging (Maybe [UnknownPath])
  | DiffCommits Commit Commit (Maybe [UnknownPath])
  | DiffCommitRange Commit Commit (Maybe [UnknownPath])
  | DiffBlobs Blob Blob
  | DiffFilesNoIndex (Path Rel File) (Path Rel File)
  deriving (Show, Eq)

data MergeSubCommand
  = MergeAbort
  | MergeContinue
  | MergePerform (Maybe Commit)
  deriving (Show, Eq)

data RebaseSubCommand
  = RebaseContinue
  | RebaseAbort
  | RebaseSkip
  | RebaseQuit
  | RebaseEditTodo
  | RebaseShowCurrentPatch
  | RebaseFromRoot (Maybe Branch)
  | RebaseFromUpstream (Maybe Branch)
  deriving (Show, Eq)

data TagSubCommand
  = TagList (Maybe Pattern)
  | TagCreate TagName (Maybe TagTarget)
  | TagVerify TagName
  | TagDelete TagName
  deriving (Show, Eq)

data FetchSubCommand
  = FetchRepository (Maybe Repository)
  | FetchGroup (Maybe Group)
  | FetchMultiple (Maybe [FetchTarget])
  | FetchAll
  deriving (Show, Eq)

data PushSubCommand
  = PushAll (Maybe PushTarget) (Maybe PushOptions)
  | PushMirror (Maybe PushTarget) (Maybe PushOptions)
  | PushTags (Maybe PushTarget) (Maybe PushOptions)
  deriving (Show, Eq)

--------------------------------------------------------------------------------
-- Options
--------------------------------------------------------------------------------
data InitOptions = InitOptions
  deriving (Show, Eq)

data CloneOptions = CloneOptions
  deriving (Show, Eq)

data AddOptions = AddOptions
  deriving (Show, Eq)

data MoveOptions = MoveOptions
  deriving (Show, Eq)

data ResetOptions = ResetOptions
  deriving (Show, Eq)

data RmOptions = RmOptions
  deriving (Show, Eq)

data GrepOptions = GrepOptions
  deriving (Show, Eq)

data LogOptions = LogOptions
  deriving (Show, Eq)

data ShowOptions = ShowOptions
  deriving (Show, Eq)

data StatusOptions = StatusOptions
  deriving (Show, Eq)

data CommitOptions = CommitOptions
  deriving (Show, Eq)

data PullOptions = PullOptions
  deriving (Show, Eq)

data PushOptions = PushOptions
  deriving (Show, Eq)

data BisectStartOptions = BisectStartOptions
  deriving (Show, Eq)
