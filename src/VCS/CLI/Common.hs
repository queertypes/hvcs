module VCS.CLI.Common where

import YNotPrelude

data Blob = Blob deriving (Show, Eq)
data Branch = Branch deriving (Show, Eq)
data Commit = Commit deriving (Show, Eq)
data Group = Group deriving (Show, Eq)
data MoveSpec = MoveSpec deriving (Show, Eq)
data Object = Object deriving (Show, Eq)
data PathSpec = PathSpec deriving (Show, Eq)
data PullSpec = PullSpec deriving (Show, Eq)
data RefSpec = RefSpec deriving (Show, Eq)
data Repository = Repository deriving (Show, Eq)
data ResetSpec = ResetSpec deriving (Show, Eq)
data Revision = Revision deriving (Show, Eq)
data RevisionRange = RevisionRange Revision Revision deriving (Show, Eq)
data StartPoint = StartPoint deriving (Show, Eq)
data Treeish = Treeish deriving (Show, Eq)
newtype Name = Name Text deriving (Show, Eq)
newtype Pattern = Pattern Text deriving (Show, Eq)
newtype ShellCommand = ShellCommand Text deriving (Show, Eq)
newtype TagName = TagName Name deriving (Show, Eq)
type LogFile = Path Rel File
type RelDir = Path Rel Dir
type RelFile = Path Rel File

data UnknownPath
  = FilePath (Path Rel File)
  | DirPathPath (Path Rel Dir)
  deriving (Show, Eq)

data PushTarget = PushTarget Repository (Maybe [RefSpec])
  deriving (Show, Eq)

data FetchTarget
  = FetchTargetRepository Repository
  | FetchTargetGroup Group
  deriving (Show, Eq)

data TagTarget
  = TagCommit Commit
  | TagObject Object
  deriving (Show, Eq)

data TermMark
  = BadTerm
  | GoodTerm
  deriving (Show, Eq)

data SkipSpec
  = SkipRevision Revision
  | SkipRange Revision Revision
  deriving (Show, Eq)
