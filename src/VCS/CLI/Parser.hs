module VCS.CLI.Parser (
  parseAdd,
  parseBisect,
  parseBranch,
  parseCheckout,
  parseClone,
  parseCommit,
  parseDiff,
  parseFetch,
  parseGrep,
  parseInit,
  parseLog,
  parseMerge,
  parseMv,
  parsePull,
  parsePush,
  parseRebase,
  parseReset,
  parseRm,
  parseShow,
  parseStatus,
  parseTag
) where

import VCS.CLI.Parser.Add
import VCS.CLI.Parser.Bisect
import VCS.CLI.Parser.Branch
import VCS.CLI.Parser.Checkout
import VCS.CLI.Parser.Clone
import VCS.CLI.Parser.Commit
import VCS.CLI.Parser.Diff
import VCS.CLI.Parser.Fetch
import VCS.CLI.Parser.Grep
import VCS.CLI.Parser.Init
import VCS.CLI.Parser.Log
import VCS.CLI.Parser.Merge
import VCS.CLI.Parser.Mv
import VCS.CLI.Parser.Pull
import VCS.CLI.Parser.Push
import VCS.CLI.Parser.Rebase
import VCS.CLI.Parser.Reset
import VCS.CLI.Parser.Rm
import VCS.CLI.Parser.Show
import VCS.CLI.Parser.Status
import VCS.CLI.Parser.Tag
