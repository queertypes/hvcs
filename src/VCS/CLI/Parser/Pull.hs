module VCS.CLI.Parser.Pull (
  parsePullOptions,
  parsePull
) where

import YNotPrelude

import Options.Applicative

import VCS.CLI.Core


parsePull :: Parser VCSCommand
parsePull =
  PullCommand
  <$> pure Nothing
  <*> optional parsePullOptions

parsePullOptions :: Parser PullOptions
parsePullOptions = pure PullOptions
