module VCS.CLI.Parser.Commit (
  parseCommitOptions,
  parseCommit
) where

import YNotPrelude

import Options.Applicative

import VCS.CLI.Core


parseCommit :: Parser VCSCommand
parseCommit =
  CommitCommand
  <$> pure Nothing
  <*> optional parseCommitOptions

parseCommitOptions :: Parser CommitOptions
parseCommitOptions = pure CommitOptions
