module VCS.CLI.Parser.Reset (
  parseResetOptions,
  parseReset
) where

import YNotPrelude

import Options.Applicative

import VCS.CLI.Common
import VCS.CLI.Core


parseReset :: Parser VCSCommand
parseReset =
  ResetCommand
  <$> pure ResetSpec
  <*> optional parseResetOptions

parseResetOptions :: Parser ResetOptions
parseResetOptions = pure ResetOptions
