module VCS.CLI.Parser.Mv (
  parseMoveOptions,
  parseMv
) where

import YNotPrelude

import Options.Applicative

import VCS.CLI.Common
import VCS.CLI.Core

parseMv :: Parser VCSCommand
parseMv =
  MvCommand
  <$> pure MoveSpec
  <*> optional parseMoveOptions

parseMoveOptions :: Parser MoveOptions
parseMoveOptions = pure MoveOptions
