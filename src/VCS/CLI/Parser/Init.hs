module VCS.CLI.Parser.Init (
  parseInitOptions,
  parseInit
) where

import YNotPrelude

import Options.Applicative

import VCS.CLI.Core

parseInit :: Parser VCSCommand
parseInit =
  InitCommand
  <$> pure Nothing
  <*> optional parseInitOptions

parseInitOptions :: Parser InitOptions
parseInitOptions = pure InitOptions
