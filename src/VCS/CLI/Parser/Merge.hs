module VCS.CLI.Parser.Merge (
  parseMerge
) where

import YNotPrelude

import Options.Applicative

import VCS.CLI.Core


parseMerge :: Parser VCSCommand
parseMerge = MergeCommand <$> pure MergeAbort
