module VCS.CLI.Parser.Add (
  parseAddOptions,
  parseAdd
) where

import YNotPrelude

import Options.Applicative

import VCS.CLI.Core

parseAdd :: Parser VCSCommand
parseAdd =
  AddCommand
  <$> pure Nothing
  <*> optional parseAddOptions

parseAddOptions :: Parser AddOptions
parseAddOptions = pure AddOptions
