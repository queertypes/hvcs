module VCS.CLI.Parser.Show (
  parseShowOptions,
  parseShow
) where

import YNotPrelude

import Options.Applicative

import VCS.CLI.Core

parseShow :: Parser VCSCommand
parseShow =
  ShowCommand
  <$> pure Nothing
  <*> optional parseShowOptions

parseShowOptions :: Parser ShowOptions
parseShowOptions = pure ShowOptions
