module VCS.CLI.Parser.Tag (
  parseTag
) where

import YNotPrelude

import Options.Applicative

import VCS.CLI.Core


parseTag :: Parser VCSCommand
parseTag = TagCommand <$> (TagList <$> pure Nothing)
