{-# LANGUAGE QuasiQuotes #-}
module VCS.CLI.Parser.Rm (
  parseRmOptions,
  parseRm
) where

import YNotPrelude

import Options.Applicative

import VCS.CLI.Core

parseRm :: Parser VCSCommand
parseRm =
  RmCommand
  <$> pure [relfile|dev/null|]
  <*> optional parseRmOptions

parseRmOptions :: Parser RmOptions
parseRmOptions = pure RmOptions
