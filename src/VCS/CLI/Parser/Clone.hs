module VCS.CLI.Parser.Clone (
  parseCloneOptions,
  parseClone
) where

import YNotPrelude

import Options.Applicative

import VCS.CLI.Common
import VCS.CLI.Core

parseCloneOptions :: Parser CloneOptions
parseCloneOptions = pure CloneOptions

parseClone :: Parser VCSCommand
parseClone =
  CloneCommand
  <$> pure Repository
  <*> pure Nothing
  <*> optional parseCloneOptions
