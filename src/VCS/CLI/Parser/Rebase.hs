module VCS.CLI.Parser.Rebase (
  parseRebase
) where

import YNotPrelude

import Options.Applicative

import VCS.CLI.Core


parseRebase :: Parser VCSCommand
parseRebase = RebaseCommand <$> pure RebaseAbort
