module VCS.CLI.Parser.Status (
  parseStatusOptions,
  parseStatus
) where

import YNotPrelude

import Options.Applicative

import VCS.CLI.Core

parseStatus :: Parser VCSCommand
parseStatus =
  StatusCommand
  <$> pure Nothing
  <*> optional parseStatusOptions

parseStatusOptions :: Parser StatusOptions
parseStatusOptions = pure StatusOptions
