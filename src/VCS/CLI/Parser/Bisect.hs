module VCS.CLI.Parser.Bisect (
  parseBisectStartOptions,
  parseBisect
) where

import YNotPrelude

import Options.Applicative

import VCS.CLI.Core


parseBisect :: Parser VCSCommand
parseBisect = BisectCommand <$> pure BisectView

parseBisectStartOptions :: Parser BisectStartOptions
parseBisectStartOptions = pure BisectStartOptions
