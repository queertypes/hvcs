module VCS.CLI.Parser.Fetch (
  parseFetch
) where

import YNotPrelude

import Options.Applicative

import VCS.CLI.Core


parseFetch :: Parser VCSCommand
parseFetch = FetchCommand <$> pure FetchAll
