module VCS.CLI.Parser.Log (
  parseLogOptions,
  parseLog
) where

import YNotPrelude

import Options.Applicative

import VCS.CLI.Core


parseLog :: Parser VCSCommand
parseLog =
  LogCommand
  <$> pure Nothing
  <*> pure Nothing
  <*> optional parseLogOptions

parseLogOptions :: Parser LogOptions
parseLogOptions = pure LogOptions
