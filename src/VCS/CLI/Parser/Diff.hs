 module VCS.CLI.Parser.Diff (
  parseDiff
) where

import YNotPrelude

import Options.Applicative

import VCS.CLI.Core


parseDiff :: Parser VCSCommand
parseDiff = DiffCommand <$> (DiffCurrent <$> pure Nothing)
