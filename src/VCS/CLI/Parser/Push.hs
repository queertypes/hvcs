module VCS.CLI.Parser.Push (
  parsePushOptions,
  parsePush
) where

import YNotPrelude

import Options.Applicative

import VCS.CLI.Core


parsePush :: Parser VCSCommand
parsePush =
  PushCommand
  <$> (PushAll
       <$> pure Nothing
       <*> optional parsePushOptions)

parsePushOptions :: Parser PushOptions
parsePushOptions = pure PushOptions
