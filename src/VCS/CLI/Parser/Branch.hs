module VCS.CLI.Parser.Branch (
  parseBranch
) where

import YNotPrelude

import Options.Applicative

import VCS.CLI.Core


parseBranch :: Parser VCSCommand
parseBranch = BranchCommand <$> pure BranchList
