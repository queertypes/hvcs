module VCS.CLI.Parser.Grep (
  parseGrepOptions,
  parseGrep
) where

import YNotPrelude

import Options.Applicative

import VCS.CLI.Core

parseGrep :: Parser VCSCommand
parseGrep =
  GrepCommand
  <$> pure Nothing
  <*> optional parseGrepOptions

parseGrepOptions :: Parser GrepOptions
parseGrepOptions = pure GrepOptions
