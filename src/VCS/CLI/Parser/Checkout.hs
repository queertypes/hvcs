module VCS.CLI.Parser.Checkout (
  parseCheckout
) where

import YNotPrelude

import Options.Applicative

import VCS.CLI.Core


parseCheckout :: Parser VCSCommand
parseCheckout = CheckoutCommand <$> (CheckoutBranch <$> pure Nothing)
