module VCS.CLI (dvcsCLI) where

import YNotPrelude hiding (FilePath)

import Options.Applicative

import VCS.CLI.Core
import VCS.CLI.Parser

dvcsCLI :: Parser VCSCommand
dvcsCLI =
  subparser
  (
    command "clone" (info (helpful parseClone) (progDesc "Clone a repository into a new directory"))
    <> command "init" (info (helpful parseInit) (progDesc "Create an empty VCS repository or reinitialize an existing one"))
    <> commandGroup "start a working area"
  )

  <|> subparser
  (
    command "add" (info (helpful parseAdd) (progDesc "Add file contents to the index"))
    <> command "mv" (info (helpful parseMv) (progDesc "Move or rename a file, directory, or symlink"))
    <> command "reset" (info (helpful parseReset) (progDesc "Reset current HEAD to the specified state"))
    <> command "rm" (info (helpful parseRm) (progDesc "Remove files from the working tree and the index"))
    <> commandGroup "work on the current changeset"
    <> hidden
  )

  <|> subparser
  (
    command "bisect" (info (helpful parseBisect) (progDesc "Use binary search to find the commit that introduced the bug"))
    <> command "grep" (info (helpful parseGrep) (progDesc "Print lines matching a pattern"))
    <> command "log" (info (helpful parseLog) (progDesc "Show commit logs"))
    <> command "show" (info (helpful parseShow) (progDesc "Show various types of objects"))
    <> command "status" (info (helpful parseStatus) (progDesc "Show the working tree status"))
    <> commandGroup "examine history and state"
    <> hidden
  )

  <|> subparser
  (
    command "branch" (info (helpful parseBranch) (progDesc "List, create, or delete branches"))
    <> command "checkout" (info (helpful parseCheckout) (progDesc "Switch branches of restore working tree files"))
    <> command "commit" (info (helpful parseCommit) (progDesc "Record changes to the repository"))
    <> command "diff" (info (helpful parseDiff) (progDesc "Show changes between commits, commit and working tree, etc."))
    <> command "merge" (info (helpful parseMerge) (progDesc "Join two or more development histories together"))
    <> command "rebase" (info (helpful parseRebase) (progDesc "Reapply commits on top of another base tip"))
    <> command "tag" (info (helpful parseTag) (progDesc "Create, list, delete, or verify a tag object signed with GPG"))
    <> commandGroup "grow, mark, and tweak your common history"
    <> hidden
  )

  <|> subparser
  (
    command "fetch" (info (helpful parseFetch) (progDesc "Download objects and refs from another repository"))
    <> command "pull" (info (helpful parsePull) (progDesc "Fetch from and integrate with another repository or a local branch"))
    <> command "push" (info (helpful parsePush) (progDesc "Update remote refs along with associated objects"))
    <> commandGroup "collaborate"
    <> hidden
  )
  where
    helpful :: Parser a -> Parser a
    helpful f = f <**> helper
